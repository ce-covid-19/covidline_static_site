# coanswers_static_site
This is the super basic starting point

# Instructions
1. Clone this repo

2. Install dependencies
`yarn install`

3. If you would like to run the app and make changes
`yarn docs:dev`

4. If you would like to build the site
`yarn docs:build`

