
module.exports = {

  locales: {
    // The key is the path for the locale to be nested under.
    // As a special case, the default locale can use '/' as its path.
    '/': {
      lang: 'en-US', // this will be set as the lang attribute on <html>
      title: 'CovidLine.org',
      description: 'Covid Line Description'
    },
    '/es/': {
      lang: 'es',
      title: '$$CovidLine.org',
      description: '$$Covid Line Description'
    }
  },

  themeConfig: {
   logo: '/assets/img/logo.png',
   search: false,
   locales: {
    // The key is the path for the locale to be nested under.
    // As a special case, the default locale can use '/' as its path.
    '/': {
      lang: 'en-US', // this will be set as the lang attribute on <html>
      // text for the language dropdown
      selectText: 'Languages',
      // label for this locale in the language dropdown
      label: 'English',
      nav: [
        { text: 'Home', link: '/' },
        { text: 'Call', link: 'tel:720-713-3452'},
        { text: 'About', link: '/about/'},
        { text: 'Colorado COVID-19', link: 'https://covid19.colorado.gov/'},
      ]
    },
    '/es/': {
      lang: 'es',
      // text for the language dropdown
      selectText: '$$Languages',
      // label for this locale in the language dropdown
      label: 'Español',
      nav: [
        { text: '$$Home', link: '/' },
        { text: '$$Call', link: 'tel:720-713-3452'},
        { text: '$$About', link: '/about/'},
        { text: '$$Colorado COVID-19', link: 'https://covid19.colorado.gov/'},
      ]
    }
   }
  }
}
