---
home: true
heroImage: /assets/img/hero.png
heroText: CovidLine.org
tagline: $$Free Info & Telehealth for all Coloradans
actionText: $$Call (720) 713-3452
actionLink: tel:720-713-3452
features:
- title: $$Free Screening
  details: $$Our automated system can quickly help to evaluate symptoms and risk factors for COVID-19.  Recommending next steps and important actions to take to protect your health.
- title: $$Quick Answers
  details: $$Discuss symptoms or concerns about the coronavirus with a trained volunteer without having to go to the doctor or hospital.
- title: $$Free Medical Advice
  details: $$Receive a free call back from a medical provider to discuss your symptoms and next steps.  Insurance not required.
footer: $$Privacy Policy | Terms of Use
---
